<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"
                omit-xml-declaration="yes"
                encoding="UTF-8"
                indent="yes"/>
    <xsl:template name="scripts">
        <script src="{$workspace}/javascript/royal-preloader.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript">

            //Replace with your own image's paths
            var images = {
            'parallax1':    '<xsl:value-of select="$workspace"/>/images/hero.jpg',
            'parallax1':    '<xsl:value-of select="$workspace"/>/images/services.jpg',
            'parallax1':    '<xsl:value-of select="$workspace"/>/images/clients.jpg'
            <!--'parallax2':    'http://www.aetherthemes.com/demo/visia/images/services-red.jpg',
            'parallax3':    'http://www.aetherthemes.com/demo/visia/images/clients.jpg',
            'About Us': 	'http://www.aetherthemes.com/demo/visia/images/about.jpg',
            'Team':     	'http://www.aetherthemes.com/demo/visia/images/team.jpg',
            'Thumb1':   	'http://www.aetherthemes.com/demo/visia/images/thumbnails/project1.jpg',
            'Thumb2':   	'http://www.aetherthemes.com/demo/visia/images/thumbnails/project2.jpg',
            'Thumb3':   	'http://www.aetherthemes.com/demo/visia/images/thumbnails/project3.jpg',
            'Thumb4':   	'http://www.aetherthemes.com/demo/visia/images/thumbnails/project4.jpg',
            'Thumb5':   	'http://www.aetherthemes.com/demo/visia/images/thumbnails/project5.jpg',
            'Thumb6':   	'http://www.aetherthemes.com/demo/visia/images/thumbnails/project6.jpg',
            'Thumb7':   	'http://www.aetherthemes.com/demo/visia/images/thumbnails/project7.jpg',
            'Thumb8':   	'http://www.aetherthemes.com/demo/visia/images/thumbnails/project8.jpg'-->
            };

            // config
            Royal_Preloader.config({
            mode:       'number',
            images:     images,
            timeout:    10,
            showInfo:   false,
            background: ['#ffffff']
            });
        </script>
        <script src="{$workspace}/javascript/smoothscroll.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/waypoints.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/parallax.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/navigation.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/jquery.mixitup.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/jquery.easing.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/jquery.fittext.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/jquery.localscroll.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/jquery.scrollto.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/jquery.appear.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/jquery.waitforimages.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/jquery.bxslider.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/jquery.fitvids.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/main.js" type="text/javascript" charset="utf-8"></script>
        <script src="{$workspace}/javascript/shortcodes.js" type="text/javascript" charset="utf-8"></script>
    </xsl:template>

</xsl:stylesheet>
