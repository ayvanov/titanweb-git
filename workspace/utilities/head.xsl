<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template name="head">
        <head>
            <meta charset="utf-8"/>
            <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
            <title>Titanweb</title>
            <meta name="description" content="Titanweb. Решения для интернета."/>
            <meta name="keywords" content="Titanweb, титанвеб"/>
            <meta name="author" content="Titanweb"/>

            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

            <link rel="shortcut icon" href="{$workspace}/images/favicon.png"/>
            <link rel="apple-touch-icon" href="{$workspace}/images/apple-touch-icon.png"/>
            <link rel="apple-touch-icon" sizes="72x72" href="{$workspace}/images/apple-touch-icon-72x72.png"/>
            <link rel="apple-touch-icon" sizes="114x114" href="{$workspace}/images/apple-touch-icon-114x114.png"/>

            <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->

            <!-- Retina Images -->
            <script>if((window.devicePixelRatio===undefined?1:window.devicePixelRatio)>1)
                document.cookie='HTTP_IS_RETINA=1;path=/';
            </script>
            <!-- End Retina Images -->

            <!-- jQuery -->
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

            <!-- Fonts -->
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300&amp;subset=cyrillic' rel='stylesheet' type='text/css'/>

            <!-- Stylesheets -->
            <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/loader.css"/>

            <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/reset.css"/>
            <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/grid.css"/>
            <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/style.css"/>


            <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/gray.css"/>
            <!-- <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/ruby-red.css"/> -->
            <!-- <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/aqua-blue.css"> -->
            <!-- <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/mint-green.css"> -->
            <!-- <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/rose-pink.css"> -->
            <!-- <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/sunflower-yellow.css"> -->
            <!-- <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/jeans.css"> -->
            <!-- <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/grape.css"> -->
            <!-- <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/grass.css"> -->
            <!-- <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/colors/lavander.css"> -->


            <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/animations.css"/>

            <!--[if lt IE 9]>
                <link rel="stylesheet" type="text/css" href="{$workspace}/stylesheets/ie.css" />
            <![endif]-->

        </head>
    </xsl:template>

</xsl:stylesheet>
