<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet
        [
                <!ENTITY nbsp   "&#160;">
                <!ENTITY mdash  "&#8212;">
                ]
        >
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html"
                omit-xml-declaration="yes"
                encoding="UTF-8"
                indent="yes"/>

    <xsl:param name="canofspam"/>
    <xsl:include href="../utilities/head.xsl"/>
    <xsl:include href="../utilities/scripts.xsl"/>
    <xsl:param name="workspace"/>
    <xsl:template match="/">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html></xsl:text>
        <html>
            <xsl:call-template name="head"/>
            <body class="royal_loader">

                <!-- Begin Navigation -->
                <nav class="clearfix">

                    <!-- Logo -->
                    <div class="logo">
                        <a id="top" href="#">
                            <img src="{$workspace}/images/logo2.png" alt="Titanweb"/>
                        </a>
                    </div>

                    <!-- Mobile Nav Button -->
                    <button type="button" class="nav-button" data-toggle="collapse" data-target=".nav-content">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Navigation Links -->
                    <div class="navigation">
                        <ul class="nav-content">
                            <li>
                                <a class="active" href="#section1">Привет</a>
                            </li>
                            <li>
                                <a href="#section2">О нас</a>
                            </li>
                            <li>
                                <a href="#section3">Услуги</a>
                            </li>
                            <li>
                                <a href="#section4">Работы</a>
                            </li>
                            <li>
                                <a id="contacts-link" href="#footer">Контакты</a>
                            </li>
                        </ul>
                    </div>

                </nav>
                <!-- End Navigation -->

                <!-- Begin Hero -->
                <section id="section1" class="parallax-bg1 hero parallax clearfix">

                    <!-- Content -->
                    <div class="dark content container">

                        <!-- Headings -->
                        <div class="ticker">
                            <h1>Креативные решения</h1>
                            <h1>Креативные идеи</h1>
                            <h1>Креативный дизайн</h1>
                        </div>

                        <!-- Buttons -->
                        <ul class="call-to-action">
                            <li>
                                <a class="button" href="#section2">Узнать больше</a>
                            </li>
                        </ul>

                    </div>

                </section>
                <!-- End Hero -->

                <!-- Begin About -->
                <section id="section2" class="content container">

                    <!-- Text -->
                    <div class="title grid-full">
                        <h2>О нас</h2>
                        <span class="border"></span>
                        <p class="sub-heading">Мы&nbsp;&mdash; Титанвеб. Мы&nbsp;предлагаем креативные решения для
                            абсолютно любых интернет проектов. Мы&nbsp;гордимся нашей работой и&nbsp;все,
                            что мы&nbsp;создаем выполняется с&nbsp;точностью и&nbsp;любовью.
                        </p>
                    </div>

                    <!-- Image -->
                    <img class="animated slide" data-appear-bottom-offset="100" src="{$workspace}/images/about.jpg"
                         alt="About Titanweb"/>
                </section>
                <!-- End About -->

                <!-- Begin Services -->
                <section id="section3" class="parallax-bg2 parallax clearfix">
                    <div class="color-overlay1S">
                        <div class="content dark padded container">

                            <!-- Text -->
                            <div class="title grid-full">
                                <h2>Услуги</h2>
                                <span class="border"></span>
                                <p class="sub-heading call-to-action">Мы&nbsp;предлагаем услуги от&nbsp;дизайна
                                    пользовательского интерфейса,
                                    до&nbsp;готовых интернет магазинов. Посмотрите на&nbsp;список наших услуг, чтобы
                                    понять,
                                    что мы&nbsp;можем сделать. Не&nbsp;забудьте посмотреть наше
                                    <a href="#section4">портфолио</a>
                                    и&nbsp;убедиться
                                    в&nbsp;качестве наших работ.
                                </p>
                            </div>

                            <!-- Icons -->
                            <ul class="icons clearfix container">
                                <!-- Single Icon -->
                                <li class="overview animated entrance" data-appear-bottom-offset="100">

                                    <!-- Feature List -->
                                    <div class="tooltip">
                                        <ul class="feature-list">
                                            <li><span class="list-dot"></span>Meta Information
                                            </li>
                                            <li><span class="list-dot"></span>Ad Words Campaign
                                            </li>
                                            <li><span class="list-dot"></span>Google Optimization
                                            </li>
                                            <li><span class="list-dot"></span>Bing Optimization
                                            </li>
                                        </ul>
                                    </div>
                                    <span class="arrow-down"></span>

                                    <!-- Icon -->
                                    <div class="icon seo"></div>

                                    <!-- Title -->
                                    <h4>Seo</h4>
                                </li>

                                <!-- Single Icon -->
                                <li class="overview animated entrance" data-appear-bottom-offset="100">

                                    <!-- Feature List -->
                                    <div class="tooltip">
                                        <ul class="feature-list">
                                            <li><span class="list-dot"></span>HTML and CSS
                                            </li>
                                            <li><span class="list-dot"></span>PHP, Ruby and Python
                                            </li>
                                            <li><span class="list-dot"></span>Object C
                                            </li>
                                            <li><span class="list-dot"></span>Javascript and jQuery
                                            </li>
                                        </ul>
                                    </div>
                                    <span class="arrow-down"></span>

                                    <!-- Icon -->
                                    <div class="icon tech"></div>

                                    <!-- Title -->
                                    <h4>Tech</h4>
                                </li>

                                <!-- Single Icon -->
                                <li class="overview animated entrance" data-appear-bottom-offset="100">

                                    <!-- Feature Lis -->
                                    <div class="tooltip">
                                        <ul class="feature-list">
                                            <li><span class="list-dot"></span>Online Shop Management
                                            </li>
                                            <li><span class="list-dot"></span>Database Support
                                            </li>
                                            <li><span class="list-dot"></span>E-commerce Cart
                                            </li>
                                        </ul>
                                    </div>
                                    <span class="arrow-down"></span>

                                    <!-- Icon -->
                                    <div class="icon commerce"></div>

                                    <!-- Title -->
                                    <h4>Commerce</h4>
                                </li>

                            </ul>
                        </div>
                    </div>
                </section>
                <!-- End Services -->

                <!-- Begin Portfolio -->
                <section id="section4" class="portfolio clearfix">

                    <!-- Title -->
                    <div class="content container">
                        <div class="title grid-full">
                            <h2>Работы</h2>
                            <span class="border"></span>
                        </div>
                        <ul class="filtering grid-full">
                            <li class="filter" data-filter="all">все</li>
                            <xsl:apply-templates select="//project-categories/entry"/>
                        </ul>
                    </div>

                    <!-- Ajax Section -->
                    <div class="ajax-section container">
                        <div class="loader"></div>
                        <div class="project-navigation">
                            <ul>
                                <li class="nextProject">
                                    <a href="#"></a>
                                </li>
                                <li class="prevProject">
                                    <a href="#"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="closeProject">
                            <a href="#">
                                <i class="icon-remove"></i>
                            </a>
                        </div>
                        <div class="ajax-content clearfix"></div>
                    </div>
                    <!-- End Ajax Section -->

                    <!-- Thumbnails -->
                    <ul id="portfolio-grid" class="projectlist clearfix">
                        <xsl:apply-templates select="//projects/entry"/>
                    </ul>

                </section>
                <!-- End Portfolio -->

                <!-- Begin Footer -->
                <footer id="footer" class="clearfix parallax-bg3">
                    <div class="content dark container">

                        <!-- Contact Links -->
                        <ul class="contact animated hatch clearfix">
                            <li class="grid-2">
                                <img src="{$workspace}/images/icons/telephone.png" alt="telephone" width="20"
                                     height="20"/>
                                <p>+ 714-555-1212</p>
                            </li>
                            <li class="grid-2">
                                <a id="contact-open" href="#">
                                    <img src="{$workspace}/images/icons/mail.png" alt="mail" width="20" height="20"/>
                                    <br/>
                                    hello@titanweb.ru
                                </a>
                            </li>
                            <li class="grid-2">
                                <a href="http://goo.gl/maps/ptykO" target="_blank">
                                    <img src="{$workspace}/images/icons/location.png" alt="location" width="20"
                                         height="20"/>
                                    <br/>
                                    1000 Coney Island Ave.<br/>Brooklyn NY 11230
                                </a>
                            </li>
                        </ul>
                    </div>

                    <!-- Contact Form -->
                    <div id="contact-form" class="dark clearfix">
                        <div class="container">
                            <div class="contact-heading grid-full">
                                <h3>Обратная связь</h3>
                                <span class="border"></span>
                            </div>
                        </div>

                        <form method="post" action="" id="contactform" class="container button" enctype="multipart/form-data">
                            <input name="MAX_FILE_SIZE" type="hidden" value="2097152" />
                            <fieldset>
                                <div class="form-field grid-half">
                                    <label for="name">Имя</label>
                                    <span>
                                        <input type="text" name="fields[name]" id="name"/>
                                    </span>
                                </div>
                                <div class="form-field grid-half">
                                    <label for="email">Email</label>
                                    <span>
                                        <input type="email" name="fields[email]" id="email"/>
                                    </span>
                                </div>
                                <div class="form-field grid-full">
                                    <label for="message">Сообщение</label>
                                    <span>
                                        <textarea name="fields[message]" id="message"></textarea>
                                    </span>
                                </div>
                            </fieldset>
                            <input name="send-email[sender-email]" value="fields[email]" type="hidden" />
                            <input name="send-email[sender-name]" value="fields[name]" type="hidden" />
                            <input name="send-email[subject]" value="Сообщение с контактной формы" type="hidden" />
                            <input name="send-email[body]" value="fields[message]" type="hidden" />
                            <input name="send-email[recipient]" value="admin" type="hidden" />
                            <input name="canofspam" value="{$canofspam}" type="hidden" />
                            <div class="form-click grid-full">
                                <span>
                                    <input type="submit" name="action[kontaktnaya-forma]" value="Отправить" id="submit"/>
                                </span>
                            </div>
                            <div id="alert" class="grid-full"></div>
                        </form>
                    </div>

                    <div class="container">

                        <!-- Copyright Info -->
                        <div class="copyright grid-full">
                            <h6>©2013 Titanweb. Все права защищены.</h6>
                        </div>

                    </div>
                </footer>
                <!-- End Footer -->

                <!-- Javascript -->
                <xsl:call-template name="scripts"/>

            </body>
        </html>
    </xsl:template>

    <xsl:template match="//projects/entry">
        <xsl:variable name="category">
            <xsl:value-of select="project-type/item"/>
        </xsl:variable>
        <xsl:variable name="category-title">
            <xsl:value-of select="//project-categories/entry[type = $category]/title"/>
        </xsl:variable>
        <li class="project mix mix_all {$category}">
            <a href="#!project/{@id}">
                <img src="{$workspace}{thumbnail/@path}/{thumbnail/filename}" alt="thumbnail"/>
                <div class="projectinfo">
                    <div class="meta">
                        <h4>
                            <xsl:value-of select="title"/>
                        </h4>
                        <h6>
                            <em>
                                <xsl:value-of select="$category-title"/>
                            </em>
                        </h6>
                    </div>
                </div>
            </a>
        </li>
    </xsl:template>

    <xsl:template match="//project-categories/entry">
        <li class="filter" data-filter="{type}">
            <xsl:value-of select="title"/>
        </li>

    </xsl:template>

</xsl:stylesheet>