<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html"
                omit-xml-declaration="yes"
                encoding="UTF-8"
                indent="yes"/>
    <xsl:param name="workspace"/>
    <xsl:template match="/">
        <!-- AJAX Content -->
        <div id="ajaxpage" class="container">

            <div class="project-hero grid-full">

                <!-- Slider -->
                <ul class="slider clearfix">
                    <li>
                        <img src="{$workspace}{//projects/entry/images/@path}/{//projects/entry/images/filename}" alt=""/>
                    </li>
                </ul>

                <!-- Pager -->
                <div class="slider-pager"></div>
                <div class="small-border"></div>

                <!-- Prev/Next -->
                <div class="project-gallery-next"></div>
                <div class="project-gallery-prev"></div>

            </div>

            <div class="project-title grid-full">
                <h4>
                    <xsl:value-of select="//projects/entry/title"/>
                </h4>
            </div>

            <div class="project-info clearfix">
                <div class="grid-full">
                    <h6>Описание</h6>
                    <p>
                        <xsl:value-of select="//projects/entry/description"/>
                    </p>
                </div>
            </div>

        </div>
        <!-- End AJAX Content -->
    </xsl:template>

</xsl:stylesheet>