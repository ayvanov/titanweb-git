<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourceproject_categories extends SectionDatasource {

		public $dsParamROOTELEMENT = 'project-categories';
		public $dsParamORDER = 'desc';
		public $dsParamPAGINATERESULTS = 'yes';
		public $dsParamLIMIT = '20';
		public $dsParamSTARTPAGE = '1';
		public $dsParamREDIRECTONEMPTY = 'no';
		public $dsParamSORT = 'system:id';
		public $dsParamASSOCIATEDENTRYCOUNTS = 'no';
		

		public $dsParamFILTERS = array(
				'5' => '{$ds-projects.project-type}',
		);
		

		public $dsParamINCLUDEDELEMENTS = array(
				'type',
				'title'
		);
		

		public function __construct($env=NULL, $process_params=true) {
			parent::__construct($env, $process_params);
			$this->_dependencies = array('$ds-projects.project-type');
		}

		public function about() {
			return array(
				'name' => 'Project categories',
				'author' => array(
					'name' => 'Alexander Ivanov',
					'website' => 'http://localhost/titanweb',
					'email' => 'xanderinho@gmail.com'),
				'version' => 'Symphony 2.3.4',
				'release-date' => '2013-11-04T17:13:54+00:00'
			);
		}

		public function getSource() {
			return '2';
		}

		public function allowEditorToParse() {
			return true;
		}

		public function execute(array &$param_pool = null) {
			$result = new XMLElement($this->dsParamROOTELEMENT);

			try{
				$result = parent::execute($param_pool);
			}
			catch(FrontendPageNotFoundException $e){
				// Work around. This ensures the 404 page is displayed and
				// is not picked up by the default catch() statement below
				FrontendPageNotFoundExceptionHandler::render($e);
			}
			catch(Exception $e){
				$result->appendChild(new XMLElement('error', $e->getMessage() . ' on ' . $e->getLine() . ' of file ' . $e->getFile()));
				return $result;
			}

			if($this->_force_empty_result) $result = $this->emptyXMLSet();

			return $result;
		}

	}
