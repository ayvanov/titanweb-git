<?php

	require_once(TOOLKIT . '/class.event.php');

	Class eventkontaktnaya_forma extends SectionEvent{

		public $ROOTELEMENT = 'kontaktnaya-forma';

		public $eParamFILTERS = array(
			'send-email',
				'xss-fail'
		);

		public static function about(){
			return array(
				'name' => 'Контактная форма',
				'author' => array(
					'name' => 'Alexander Ivanov',
					'website' => 'http://titanweb.ru',
					'email' => 'xanderinho@gmail.com'),
				'version' => 'Symphony 2.3.4',
				'release-date' => '2013-11-08T05:17:47+00:00',
				'trigger-condition' => 'action[kontaktnaya-forma]'
			);
		}

		public static function getSource(){
			return '3';
		}

		public static function allowEditorToParse(){
			return true;
		}

		public static function documentation(){
			return '
        <h3>Примеры успешного и неудачного XML кода</h3>
        <p>При успешном сохранении будет возвращен следующий XML:</p>
        <pre class="XML"><code>&lt;kontaktnaya-forma result="success" type="create | edit">
  &lt;message>Запись [создана | отредактирована] успешно.&lt;/message>
&lt;/kontaktnaya-forma></code></pre>
        <p>Во время сохранения возникли ошибки из-за отсутствующих или неверных полей, слудющий XML код будет возвращён:</p>
        <pre class="XML"><code>&lt;kontaktnaya-forma result="error">
  &lt;message>При сохранении записи возникли ошибки.&lt;/message>
  &lt;field-name type="invalid | missing" />
  ...
&lt;/kontaktnaya-forma></code></pre>
        <p>Это пример кода возвращаемого, если хотя бы один из фильтров не сработает:</p>
        <pre class="XML"><code>&lt;kontaktnaya-forma result="error">
  &lt;message>При сохранении записи возникли ошибки.&lt;/message>
  &lt;filter name="admin-only" status="failed" />
  &lt;filter name="send-email" status="failed">Получатель не найден&lt;/filter>
  ...
&lt;/kontaktnaya-forma></code></pre>
        <h3>Пример кода формы для клиентской страницы</h3>
        <p>Это пример кода формы, который вы можете использовать в клиентской части:</p>
        <pre class="XML"><code>&lt;form method="post" action="" enctype="multipart/form-data">
  &lt;input name="MAX_FILE_SIZE" type="hidden" value="2097152" />
  &lt;label>Имя
    &lt;input name="fields[name]" type="text" />
  &lt;/label>
  &lt;label>Email
    &lt;input name="fields[email]" type="text" />
  &lt;/label>
  &lt;label>Сообщение
    &lt;textarea name="fields[message]" rows="15" cols="50">&lt;/textarea>
  &lt;/label>
  &lt;input name="action[kontaktnaya-forma]" type="submit" value="Принять" />
&lt;/form></code></pre>
        <p>Чтобы отредактировать существующую запись включите в форму ID записи. Лучше всего это сделать следующим образом:</p>
        <pre class="XML"><code>&lt;input name="id" type="hidden" value="23" /></code></pre>
        <p>Для переадресации по новому адресу после успешного сохранения включите в форму ссылку на место переадресации. Лучше все это сделать при помощи скрытого поля, например таким образом, указав в качестве value URL:</p>
        <pre class="XML"><code>&lt;input name="redirect" type="hidden" value="http://titanweb.ru/success/" /></code></pre>
        <h3>Отправить уведомление на почту</h3>
        <p>В случае успешного сохранения записи, данная опция позволит получить данные из формы и отправить их на почту заданному получателю. <strong>Работа с ‘Allow Multiple’ пока невозможна</strong>. Следующие поля являются определенными:</p>
        <pre class="XML"><code>send-email[sender-email] // Опционально
send-email[sender-name] // Опционально
send-email[reply-to-email] // Опционально
send-email[reply-to-name] // Опционально
send-email[subject]
send-email[body]
send-email[recipient] // список имен авторов, разделённых запятыми.</code></pre>
        <p>Все эти поля могут быть созданы динамически с использованием названия другого поля формы, как показано ниже в примере:</p>
        <pre class="XML"><code>&lt;form action="" method="post">
    &lt;fieldset>
      &lt;label>Имя &lt;input type="text" name="fields[author]" value="" />&lt;/label>
      &lt;label>Email &lt;input type="text" name="fields[email]" value="" />&lt;/label>
      &lt;label>Сообщение &lt;textarea name="fields[message]" rows="5" cols="21">&lt;/textarea>&lt;/label>
      &lt;input name="send-email[sender-email]" value="fields[email]" type="hidden" />
      &lt;input name="send-email[sender-name]" value="fields[author]" type="hidden" />
      &lt;input name="send-email[reply-to-email]" value="fields[email]" type="hidden" />
      &lt;input name="send-email[reply-to-name]" value="fields[author]" type="hidden" />
      &lt;input name="send-email[subject]" value="You are being contacted" type="hidden" />
      &lt;input name="send-email[body]" value="fields[message]" type="hidden" />
      &lt;input name="send-email[recipient]" value="fred" type="hidden" />
      &lt;input id="submit" type="submit" name="action[save-contact-form]" value="Send" />
    &lt;/fieldset>
  &lt;/form></code></pre>';
		}

		public function load(){
			if(isset($_POST['action']['kontaktnaya-forma'])) return $this->__trigger();
		}

	}
